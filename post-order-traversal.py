from collections import deque
from typing import List


def post_order_traversal(tree: List[List], accumulator: List) -> List:  # needs updation in the PDf, it says tree: dict which isn't
    val, others = tree[0], tree[1:]

    for each in others:
        post_order_traversal(each, accumulator)

    accumulator.append(val)
    return accumulator


tree1 = [1, [2, [4], [5]], [3]]
tree2 = ['f', ['b', ['a'], ['d', ['c'], ['e']]], ['g', ['i', ['h']]]]
tree3 = ['re', ['b', ['orn'], ['ate']], ['alize', ['s']], ['lief'], ['d', ['der']]]
assert post_order_traversal(tree1, []) == [4, 5, 2, 3, 1]
assert post_order_traversal(tree2, []) == ['a', 'c', 'e', 'd', 'b', 'h', 'i', 'g', 'f']
assert post_order_traversal(tree3, []) == ['orn', 'ate', 'b', 's', 'alize', 'lief', 'der', 'd', 're']
