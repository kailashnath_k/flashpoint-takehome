'''
Algorithm: As the test cases use data of alpha numerical type I've chosen to take the assumption that we will not support special characters in the ascii range (33 to 42) and use those as replacement for 0 to 9

In choosing so we can deal with numerical values as part of the encrypted string

'''


def is_number(char):
    ''' Returns if a given character has ascii code between those of 48 and 57 (0 - 9) '''
    return 48 <= ord(char) <= 57


def is_encrypted_number(char):
    ''' Returns if a given character has ascii code between those of 33 and 42 (! - *) '''
    return 33 <= ord(char) <= 42


def compress(input: str) -> str:
    assert input is not None, "Input cannot be None"
    assert type(input) == str, "Can compress only a string"

    # in the first sweep we replace all numericals with those of special characters
    string = ''.join([chr(ord(e) - 15) if 48 <= ord(e) <= 57 else e for e in input])

    compressed_string = ''
    i = 1

    # by default every character has an occurence of atleast once
    counter = 1

    while i < len(string):
        if string[i - 1] != string[i]:
            compressed_string += string[i - 1]
            if counter > 1:
                compressed_string += '%d' % counter
                counter = 1
        else:
            counter += 1

        i += 1
    else:
        # since i is one step ahead and we only add i - 1 to compressed_string we add the last character after the loop exits
        compressed_string += string[-1]
        if counter > 1:
            compressed_string += str(counter)

    return compressed_string


def uncompress(compressed_string: str) -> str:

    assert compressed_string is not None, "Input cannot be None"
    assert type(compressed_string) == str, "Can try decompressing only a string"

    uncompressed_string = ''
    i = 1

    while i < len(compressed_string):
        count = 0
        j = i

        # we run an inner loop to find if the current character is a number
        # and if a number what is it; Loop is to identify numbers which are > 9 as the number of characters
        # in that case is usually dynamic; can range from (10 - infinity); Here infinity = sys.maxint
        while j < len(compressed_string):
            possible_number = compressed_string[i: j + 1]
            int_at_j = compressed_string[j]

            if not is_number(int_at_j):
                j = 0
                break
            else:
                count = int(possible_number)

            j += 1

        if count > 0:
            uncompressed_string += compressed_string[i - 1] * count
            # we need to move the i to the next character after the number
            i += 1 + (1 if count < 10 else len(str(count)))
        else:
            uncompressed_string += compressed_string[i - 1]
            i += 1
    else:
        if count == 0:
            uncompressed_string += compressed_string[-1]

    return ''.join([chr(ord(e) + 15) if is_encrypted_number(e) else e for e in uncompressed_string])


strings = {
    '0DE9MDK9J8I1BMUQ18HARUPOKXFE4HLADWV12OYYTUFI59Y1',
    '6QXXCOLMUNBLYY0WOB5BR2HIR5L5XG02TGRAGV',
    '5PNL',
    'GKF8ANZ2DH6P3B5WWFMELX8XEMRSJGKHMDN932EZTM2O',
    '4ZILNB9DW3Y65GIG4Z5WWICIJN6H7HTU88',
    'Aaaaahhhhhhmmmmmmmuiiiiiiiaaaaaa',
    'WWWWWWWWWWWWBWWWWWWWWWWWWBBBWWWWWWWWWWWWWWWWWWWWWWWWBWWWWWWWWWWWWWW',
    'kk20',
    'kkkker3335kkk',
}

for string in strings:
    if not string:
        continue
    print('Input: %s; Size: %d' % (string, len(string)))

    compressed = compress(string)
    print('Compressed: %s; Size: %d' % (compressed, len(compressed)))

    uncompressed = uncompress(compressed)
    print('Uncompressed: %s; Size: %d' % (uncompressed, len(uncompressed)))

    compression_percentage = ((len(string) - len(compressed)) / len(string) * 1.0)
    print('Compression ratio: %.2f' % compression_percentage)
    print('-' * 50)
