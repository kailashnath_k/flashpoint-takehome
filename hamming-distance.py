from collections import Iterable


def hamming_distance(v1, v2):
    assert isinstance(v1, Iterable), "Argument v1 is not iterable"
    assert isinstance(v2, Iterable), "Argument v2 is not iterable"
    assert len(v1) == len(v2), "Invalid lengths"

    index = 0
    distance = 0
    while index < len(v1):
        if v1[index] != v2[index]:
            distance += 1

        index += 1

    return distance


assert hamming_distance([], []) == 0
assert hamming_distance([0, 1], [0, 1]) == 0
assert hamming_distance("00", "01") == 1
assert hamming_distance("karolin", "kathrin") == 3
assert hamming_distance("karolin", "kerstin") == 3
assert hamming_distance((1, 0, 1, 1, 1, 0, 1), (1, 0, 0, 1, 0, 0, 1)) == 2
assert hamming_distance("2173896", "2233796") == 3
try:
    hamming_distance("size2", "size24")
except AssertionError as aE:
    assert aE.message == "Invalid lengths"

try:
    hamming_distance(123, 123)
except AssertionError as aE:
    assert aE.message == "Argument v1 is not iterable"
