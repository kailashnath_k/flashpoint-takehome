-- Deduplicate
-- create table users (Id tinyint, Email varchar(65));

select Email from users group by Email having count(Email) > 1;


-- Ranks; (Have never used auto incrementing row_number's before, hence googled)
-- create table ranks (Id tinyint, Score double(3,2));

select R.score as Score, (@rank := @rank + 1) from ranks R, (select @rank := 0) _ order by R.score desc;
